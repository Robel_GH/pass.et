package com.malamengineering.passet

import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v4.widget.DrawerLayout
import android.support.design.widget.NavigationView
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View

class MainActivity : AppCompatActivity(){

    lateinit var drawerLayout: DrawerLayout
    lateinit var navigationView: NavigationView
    lateinit var fragmentManager: FragmentManager
    lateinit var fragmentTransaction:FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        drawerLayout= findViewById<View>(R.id.drawerLayout) as DrawerLayout
        navigationView= findViewById<View>(R.id.navView) as NavigationView
        fragmentManager=supportFragmentManager
        fragmentTransaction=fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerView,TabLayout()).commit()



        navigationView.setNavigationItemSelectedListener {
            
            menuItem->drawerLayout.closeDrawers()

            if (menuItem.itemId==R.id.navHome){
                val ft=fragmentManager.beginTransaction()
                ft.replace(R.id.containerView,TabLayout()).commit()

            }
            if(menuItem.itemId==R.id.navExamDays){
                val ft=fragmentManager.beginTransaction()
                ft.replace(R.id.containerView,ExamDays()).commit()
            }
            if(menuItem.itemId==R.id.navPurchasedQuestions){
                val ft=fragmentManager.beginTransaction()
                ft.replace(R.id.containerView,PurchasedQuestions()).commit()
            }
            if(menuItem.itemId==R.id.navUpdateContent){
                val ft=fragmentManager.beginTransaction()
                ft.replace(R.id.containerView,UpdateContent()).commit()
            }
            if(menuItem.itemId==R.id.navTools){
                val ft=fragmentManager.beginTransaction()
                ft.replace(R.id.containerView,Tools()).commit()
            }
                false
        }

        val toolbar=findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        val drawerToggle=ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
    }

//    override fun onBackPressed() {
//        drawerLayout=findViewById(R.id.drawer_layout)
//        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
//            drawerLayout.closeDrawer(GravityCompat.START)
//        } else {
//            super.onBackPressed()
//        }
//    }


}
