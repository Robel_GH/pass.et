package com.malamengineering.passet

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class TabLayout: Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val x=inflater.inflate(R.layout.tab_layout,null)

        tabLayout=x.findViewById<View>(R.id.tabs) as TabLayout
        viewPager=x.findViewById<View>(R.id.viewPager)as ViewPager

        viewPager.adapter=MyAdapter(childFragmentManager)
        tabLayout.post { tabLayout.setupWithViewPager(viewPager) }

        return x
    }

    internal inner class MyAdapter(fm:FragmentManager):FragmentPagerAdapter(fm){

        override fun getItem(p0: Int): Fragment? {
            when(p0){
                0->return AllTab()
                1->return EntranceTab()
                2->return DrivingLicenseTab()
                3->return JobTab()
                4->return OtherTab()
            }
            return null
        }

        override fun getCount(): Int {
            return int_items
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when(position){
                0->return "All"
                1->return "Entrance"
                2->return "Driving License"
                3->return "Job"
                4->return "Other"
            }
            return null
        }
    }

    companion object {

        lateinit var tabLayout: TabLayout
        lateinit var viewPager: ViewPager
        var int_items=5

    }
}

